
import {Fragment} from 'react';


import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';

function App() {

  /*const name = 'John Smith';
  const element = <h1>Hello, {name}</h1>;*/


  return (
    <Fragment>
      <AppNavbar/>
      {/* <Home/>
      <Courses/> */}
      {/* <Register/> */}
      <Login/>
    </Fragment>
  );
}

export default App;
