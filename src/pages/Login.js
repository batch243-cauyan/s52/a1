import { useState, useEffect } from 'react';
import {Button, Form, Col, Row} from 'react-bootstrap';


const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);
    

    useEffect(()=>{
        if (email && password){
            setIsActive(true);
        }else{
            setIsActive(false)
    }
    },[email,password])


    function loginUser(event){
        event.preventDefault();
        alert("Logged In Successfully.")
        setEmail('');
        setPassword('');
    }
    return ( 
      
        <Row>
            <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                <Form className='p-3' onSubmit={loginUser}>
                    <h1>Login</h1>
                    <Form.Group className="mb-3" controlId="email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" required value={email} onChange={event =>{
                            setEmail(event.target.value)
                        }}/>
                        
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" required value={password} onChange={event =>{
                            setPassword(event.target.value)
                        }}/>

                    </Form.Group>

                    <Button className ="btn-lg"variant="success" type="submit" disabled={!isActive} >
                        Login
                    </Button>
                </Form>
        </Col>
    </Row>
     );
}
 
export default Login;