import {Fragment} from 'react'
import CourseCard from '../components/CourseCard';
import courseData from '../data/courses.js';

export default function Courses(){
	

	/*console.log(courseData);*/

	const courses = courseData.map(course => {
		return(
				<CourseCard key = {course.id} prop ={course}/>
			)
	})

	return(
		<Fragment>
			{courses}
		</Fragment>
		)
}